#!/bin/sh

set -x

DIRNAME=`dirname $0`
# Suck in a ton of profile settings from our parent's setup scripts.
. $DIRNAME/../../setup-lib.sh

# Grab our openstack creds.
. $OURDIR/admin-openrc.sh

# Set some local parameter defaults, before we suck in the params from
# the profile.
ONAP_PUBKEY=""
ONAP_EXT_NET="flat-lan-1-net"
ONAP_REPLICAS=1
ONAP_INTEGRATION_REPO="https://gitlab.flux.utah.edu/johnsond/onap-integration.git"
ONAP_INTEGRATION_BRANCH="powder-casablanca"
ONAP_DEMO_REPO="https://gitlab.flux.utah.edu/johnsond/onap-demo.git"
ONAP_DEMO_BRANCH="powder-casablanca"
ONAP_PRELOAD_DEMOS=0

# Grab our parameters.
. $OURDIR/parameters

# Local vars.
ONAP_ENV=$OURDIR/onap/onap-oom.env

# Local functions.
__openstack() {
    __err=1
    __debug=
    __times=0
    while [ $__times -lt 16 -a ! $__err -eq 0 ]; do
        openstack $__debug "$@"
        __err=$?
        if [ $__err -eq 0 ]; then
            break
        fi
        __debug=" --debug "
        __times=`expr $__times + 1`
        if [ $__times -gt 1 ]; then
            echo "ERROR: openstack command failed: sleeping and trying again!"
            sleep 8
        fi
    done
}

mkdir -p $OURDIR/onap
cd $OURDIR/onap

HDOMAIN=`hostname`

#
# First, change all the public-facing service endpoints to our FQDN.  We
# really should be doing this in the OpenStack profile too; but ONAP
# requires it.
#
__openstack endpoint list | grep -q public
if [ $? -eq 0 ]; then
    IFS='
'
    for line in `openstack endpoint list | grep public`; do
	sid=`echo $line | awk '/ / {print $2}'`
	surl=`echo $line | sed -e 's/^.*\(http[^ ]*\) *.*$/\1/'`
	url=`echo "$surl" | sed -e "s/$CONTROLLER:/$HDOMAIN:/"`
	echo "Changing public endpoint ($surl) to ($url)"
	__openstack endpoint set --url "$url" "$sid"
    done
    unset IFS
else
    for sid in `openstack endpoint list | awk '/ / {print \$2}' | grep -v ^ID\$`; do
	surl=`openstack endpoint show $sid | grep publicurl | awk '/ / {print \$4}'`
	if [ -n "$surl" ]; then
	    url=`echo "$surl" | sed -e "s/$CONTROLLER:/$HDOMAIN:/"`
	    echo "Changing public endpoint ($surl) to ($url)"
	    echo "update endpoint set url='$url' where interface='public' and legacy_endpoint_id='$sid'" \
	        | mysql keystone
	fi
    done
fi

#
# Also, change the OS_AUTH_URL value to point to the FQDN, for later use
# in the ONAP env file.
#
export OS_AUTH_URL=`echo "$OS_AUTH_URL" | sed -e "s/$CONTROLLER:/$HDOMAIN:/"`

#
# Change the RAM policy *not* to overcommit.  At least some of the ONAP
# VMs need what they ask for.
#
crudini --set /etc/nova/nova.conf DEFAULT ram_allocation_ratio 1.0
systemctl restart nova-api nova-scheduler nova-conductor

#
# Enable the neutron metadata service for router-less tenant networks.
#
crudini --set /etc/neutron/dhcp_agent.ini DEFAULT enable_isolated_metadata true
systemctl restart neutron-l3-agent neutron-dhcp-agent

#
# Set up a reverse proxy for the Rancher/k8s UI; it redirects to the
# floating IP of the Rancher VM.  We need to do this whether or not
# $ONAP_EXT_NET is ext-net, because the Rancher install does not
# configure local auth, so it's wide open.  In fact, in the case that
# $ONAP_EXT_NET is the ext-net, we install a security group rule
# prohibiting that IP:8080 being reached from outside the control
# netmask.
#
maybe_install_packages nginx
if [ ! $? -eq 0 ]; then
    rm -f /etc/nginx/sites-available/default /etc/nginx/sites-enabled/default
    maybe_install_packages nginx
fi
if [ $OSVERSION -lt $OSQUEENS ]; then
    maybe_install_packages software-properties-common python-software-properties
    add-apt-repository -y ppa:certbot/certbot
    apt-get -y update
fi
maybe_install_packages python-certbot-apache
BOSSNODE=`cat /etc/emulab/bossnode`
if [ -n "$BOSSNODE" ]; then
    SWAPPER_EMAIL=`geni-get -s $BOSSNODE slice_email`
else
    SWAPPER_EMAIL=`geni-get slice_email`
fi
certbot certonly -d $HDOMAIN --apache --agree-tos -m "$SWAPPER_EMAIL" -n
mkdir /etc/nginx/ssl
cp -p /etc/letsencrypt/live/$HDOMAIN/*.pem /etc/nginx/ssl/
chown -R www-data:root /etc/nginx/ssl/
chmod 770 /etc/nginx/ssl

echo "$ADMIN_PASS" | htpasswd -n -i admin > /etc/nginx/htpasswd
chown www-data:root /etc/nginx/htpasswd
chmod 660 /etc/nginx/htpasswd

rm -f /etc/nginx/sites-enabled/default
cat <<EOF >/etc/nginx/sites-available/rancher-reverse-proxy
map \$http_upgrade \$connection_upgrade {
        default Upgrade;
        ''      close;
}
server {
        listen 4443 ssl http2;
        listen [::]:4443 ssl http2;
        #gzip off;
        server_name $HDOMAIN;
        ssl_certificate /etc/nginx/ssl/fullchain.pem;
        ssl_certificate_key /etc/nginx/ssl/privkey.pem;
        location / {
                proxy_set_header Host \$host;
                proxy_set_header X-Forwarded-Proto \$scheme;
                proxy_set_header X-Forwarded-Port \$server_port;
                proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
                #proxy_set_header X-Real-IP \$remote_addr;
                proxy_pass http://RANCHERIP:8080;
                proxy_http_version 1.1;
                proxy_set_header Upgrade \$http_upgrade;
                proxy_set_header Connection \$connection_upgrade;
                proxy_read_timeout 900s;
                auth_basic "rancher";
                auth_basic_user_file /etc/nginx/htpasswd;
                proxy_set_header Authorization "";
        }
}
EOF
systemctl restart nginx
systemctl enable nginx

# Fork a job to wait for the oom-rancher VM to get a floating IP.
cat <<'EOF' | sh >$OURDIR/setup-ext-onap-nginx-rancher.log 2>&1 &
set -x
. /root/setup/admin-openrc.sh
while [ true ]; do
    #RANCHER_FLOATING_IP=`openstack server list | awk '/ oom-rancher / { print $9 }'`
    RANCHER_FLOATING_IP=`openstack stack output show oom rancher_vm_ip -c output_value -f value`
    echo "$RANCHER_FLOATING_IP" | grep -q '[0-9]*\.[0-9]*\.[0-9]*\.[0-9]*'
    if [ $? -eq 0 ]; then
        break;
    fi
    sleep 8;
done
sed -i /etc/nginx/sites-available/rancher-reverse-proxy -e "s/RANCHERIP/$RANCHER_FLOATING_IP/"
ln -s /etc/nginx/sites-available/rancher-reverse-proxy /etc/nginx/sites-enabled/
systemctl restart nginx
EOF

#
# We already install and run bind9 as a recursive resolver to support
# Designate in OpenStack.  So, we add a little fake zone for the ONAP
# names, and allow the nameserver to listen publicly on :4453 .  We
# spawn a job to actually add the names and HUP bind.  In the
# $ONAP_EXT_NET == ext-net case, all the names get the value of the
# external IP of the VM hosting k8s NodePorts; in the case where
# $ONAP_EXT_NET != ext-net, all the names are trivially $MYIP (our
# external IP), because we proxy all ONAP services from $MYIP to the
# flat-lan-1-net "external" network.
#
# Well, we don't do this in older releases, so make sure it's installed.
maybe_install_packages bind9
cat <<EOF >/etc/bind/db.onap
\$TTL    60
@       IN      SOA     ns.simpledemo.onap.org. root.simpledemo.onap.org. (
                              1         ; Serial
                         604800         ; Refresh
                          86400         ; Retry
                        2419200         ; Expire
                         604800 )       ; Negative Cache TTL

               NS      ns
       IN      NS      ns.simpledemo.onap.org.
       IN      A       $MYIP
ns     IN      A       $MYIP

ns.simpledemo.onap.org.          IN   A   $MYIP
EOF
sed -i -e "s/^ *options *{/options {\n    listen-on port 4453 { 127.0.0.1; $MGMTIP; $MYIP; };/" /etc/bind/named.conf.options
sed -i -e "s/allow-query {/allow-query { any;/" /etc/bind/named.conf.options
cat <<EOF >>/etc/bind/named.conf.local
zone "simpledemo.onap.org" {
    type master;
    file "/etc/bind/db.onap";
};
EOF
#echo "/etc/bind/db.onap" >> /etc/bind/named.conf.local
systemctl restart bind9

if [ $ONAP_EXT_NET = "ext-net" ]; then
    # Wait for the portal pod to appear and get an external IP.
    cat <<'EOF' | sh >$OURDIR/setup-ext-onap-bind-portal.log 2>&1 &
set -x
. /root/setup/admin-openrc.sh
while [ true ]; do
    #RANCHER_FLOATING_IP=`openstack server list | awk '/ oom-rancher / { print $9 }'`
    RANCHER_FLOATING_IP=`openstack stack output show oom rancher_vm_ip -c output_value -f value`
    echo "$RANCHER_FLOATING_IP" | grep -q '[0-9]*\.[0-9]*\.[0-9]*\.[0-9]*'
    if [ ! $? -eq 0 ]; then
        sleep 8;
        continue;
    fi
    VM_IP=`ssh -i ~/.ssh/onap_key ubuntu@$RANCHER_FLOATING_IP sudo -i -u root kubectl get svc -n onap | sed -ne 's/^portal-app[ ]*LoadBalancer[ ]*[0-9\.]*[ ]*\([0-9\.]*\)[ ]*.*$/\1/p'`
    echo "$VM_IP" | grep -q '[0-9]*\.[0-9]*\.[0-9]*\.[0-9]*'
    if [ ! $? -eq 0 ]; then
        sleep 8;
        continue;
    fi
    PORTAL_IP=`openstack server list | sed -ne "s/^.*=${VM_IP},[ ]*\([0-9\.]*\)[ ]*.*$/\1/p"`
    echo "$PORTAL_IP" | grep -q '[0-9]*\.[0-9]*\.[0-9]*\.[0-9]*'
    if [ ! $? -eq 0 ]; then
        sleep 8;
        continue;
    fi
    break;
done
echo "*.simpledemo.onap.org.   IN   A   $PORTAL_IP" >>/etc/bind/db.onap
systemctl restart bind9
EOF
else
    echo "*.simpledemo.onap.org.   IN   A   $MYIP" >>/etc/bind/db.onap
    systemctl restart bind9
fi

#
# Always install a secure (letsencrypt) proxy to the ONAP Portal.  This
# allows us to give users a clickable URL in the Cloudlab/POWDER portal
# UI.  They still have to figure out which floating IP the
# *.simpledemo.onap.org names should point to, though.
#
cat <<EOF >/etc/nginx/sites-available/portal-reverse-proxy
map \$http_upgrade \$connection_upgrade {
        default Upgrade;
        ''      close;
}
server {
        listen 8989 ssl http2;
        listen [::]:8989 ssl http2;
        #gzip off;
        server_name $HDOMAIN;
        ssl_certificate /etc/nginx/ssl/fullchain.pem;
        ssl_certificate_key /etc/nginx/ssl/privkey.pem;
        location / {
                proxy_set_header Host \$host;
                proxy_set_header X-Forwarded-Proto \$scheme;
                proxy_set_header X-Forwarded-Port \$server_port;
                proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
                proxy_set_header X-Real-IP \$remote_addr;
                proxy_pass http://PORTALIP:8989;
                proxy_http_version 1.1;
                proxy_set_header Upgrade \$http_upgrade;
                proxy_set_header Connection \$connection_upgrade;
                proxy_read_timeout 900s;
                #auth_basic "portal";
                #auth_basic_user_file /etc/nginx/htpasswd;
                #proxy_set_header Authorization "";
        }
}
EOF

#
# If we're using the flat lan as the ONAP ext net (the default config),
# we need to adjust the base openstack network config.  We have to make
# flat-lan-1-net an external network (but first, tell the l3 agent to
# worry about specific external nets and restart neutron-l3-agent
# neutron-openvswitch-agent|neutron-linuxbridge-agent neutron-server)
#
#
if [ "$ONAP_EXT_NET" != "ext-net" ]; then
    crudini --del /etc/neutron/l3_agent.ini DEFAULT external_network_bridge
    agent=`(. $OURDIR/parameters ; echo $ML2PLUGIN)`
    systemctl restart neutron-l3-agent neutron-${agent}-agent neutron-server
    openstack network set --external "$ONAP_EXT_NET"
    # Could do this for tun0-net, for proof-of-concept.
    #neutron router-gateway-clear tun0-net
    #openstack router set --external-gateway "$ONAP_EXT_NET" tun0-router
    cat <<EOF >>/etc/nginx/sites-available/portal-reverse-proxy
# Web Portal, policy thing -- ssl inside.
server {
        listen 30219 ssl http2;
        listen 30200 ssl http2;
        listen 30225 ssl http2;
        #gzip off;
        server_name $HDOMAIN;
        ssl_certificate /etc/nginx/ssl/fullchain.pem;
        ssl_certificate_key /etc/nginx/ssl/privkey.pem;
        location / {
                proxy_set_header Host \$host;
                proxy_set_header X-Forwarded-Proto \$scheme;
                proxy_set_header X-Forwarded-Port \$server_port;
                proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
                proxy_set_header X-Real-IP \$remote_addr;
                proxy_pass https://PORTALIP:\$server_port;
                proxy_ssl_verify off;
                proxy_http_version 1.1;
                proxy_set_header Upgrade \$http_upgrade;
                proxy_set_header Connection \$connection_upgrade;
                proxy_read_timeout 900s;
                #auth_basic "portal";
                #auth_basic_user_file /etc/nginx/htpasswd;
                #proxy_set_header Authorization "";
        }
}
# Anything else -- assume http outside, http inside
server {
        listen 30201 ssl http2;
        listen 30202 ssl http2;
        listen 30203 ssl http2;
        listen 30204 ssl http2;
        listen 30205 ssl http2;
        listen 30206 ssl http2;
        listen 30207 ssl http2;
        listen 30208 ssl http2;
        listen 30209 ssl http2;
        listen 30210 ssl http2;
        listen 30211 ssl http2;
        listen 30212 ssl http2;
        listen 30213 ssl http2;
        listen 30214 ssl http2;
        listen 30215 ssl http2;
        listen 30216 ssl http2;
        listen 30217 ssl http2;
        listen 30218 ssl http2;
        listen 30220 ssl http2;
        listen 30221 ssl http2;
        listen 30222 ssl http2;
        listen 30223 ssl http2;
        listen 30224 ssl http2;
        listen 30225 ssl http2;
        listen 30226 ssl http2;
        listen 30227 ssl http2;
        listen 30228 ssl http2;
        listen 30229 ssl http2;
        listen 30230 ssl http2;
        listen 30231 ssl http2;
        listen 30232 ssl http2;
        listen 30233 ssl http2;
        listen 30234 ssl http2;
        listen 30235 ssl http2;
        listen 30236 ssl http2;
        listen 30237 ssl http2;
        listen 30238 ssl http2;
        listen 30239 ssl http2;
        listen 30240 ssl http2;
        listen 30241 ssl http2;
        listen 30242 ssl http2;
        listen 30243 ssl http2;
        listen 30244 ssl http2;
        listen 30245 ssl http2;
        listen 30246 ssl http2;
        listen 30247 ssl http2;
        listen 30248 ssl http2;
        listen 30249 ssl http2;
        listen 30250 ssl http2;
        listen 30251 ssl http2;
        listen 30252 ssl http2;
        listen 30253 ssl http2;
        listen 30254 ssl http2;
        listen 30255 ssl http2;
        listen 30256 ssl http2;
        listen 30257 ssl http2;
        listen 30258 ssl http2;
        listen 30259 ssl http2;
        listen 30260 ssl http2;
        listen 30261 ssl http2;
        listen 30262 ssl http2;
        listen 30263 ssl http2;
        listen 30264 ssl http2;
        listen 30265 ssl http2;
        listen 30266 ssl http2;
        listen 30267 ssl http2;
        listen 30268 ssl http2;
        listen 30269 ssl http2;
        listen 30270 ssl http2;
        listen 30272 ssl http2;
        listen 30273 ssl http2;
        listen 30274 ssl http2;
        listen 30275 ssl http2;
        listen 30276 ssl http2;
        listen 30277 ssl http2;
        listen 30278 ssl http2;
        listen 30279 ssl http2;
        listen 30280 ssl http2;
        listen 30281 ssl http2;
        listen 30282 ssl http2;
        listen 30283 ssl http2;
        listen 30284 ssl http2;
        listen 30285 ssl http2;
        listen 30286 ssl http2;
        listen 30287 ssl http2;
        listen 30288 ssl http2;
        listen 30289 ssl http2;
        listen 30290 ssl http2;
        listen 30291 ssl http2;
        listen 30292 ssl http2;
        listen 30293 ssl http2;
        listen 30294 ssl http2;
        listen 30295 ssl http2;
        listen 30296 ssl http2;
        listen 30297 ssl http2;
        listen 30298 ssl http2;
        listen 30299 ssl http2;

        #gzip off;
        server_name $HDOMAIN;
        ssl_certificate /etc/nginx/ssl/fullchain.pem;
        ssl_certificate_key /etc/nginx/ssl/privkey.pem;
        location / {
                proxy_set_header Host \$host;
                proxy_set_header X-Forwarded-Proto \$scheme;
                proxy_set_header X-Forwarded-Port \$server_port;
                proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
                proxy_set_header X-Real-IP \$remote_addr;
                proxy_pass http://PORTALIP:\$server_port;
                #proxy_ssl_verify off;
                proxy_http_version 1.1;
                proxy_set_header Upgrade \$http_upgrade;
                proxy_set_header Connection \$connection_upgrade;
                proxy_read_timeout 900s;
                #auth_basic "portal";
                #auth_basic_user_file /etc/nginx/htpasswd;
                #proxy_set_header Authorization "";
        }
}
EOF
fi

#
# Wait for the ONAP portal-app pod to appear and get an external IP, and
# update the nginx proxy config.
#
cat <<'EOF' | sh >$OURDIR/setup-ext-onap-nginx-portal.log 2>&1 &
set -x
. /root/setup/admin-openrc.sh
while [ true ]; do
    #RANCHER_FLOATING_IP=`openstack server list | awk '/ oom-rancher / { print $9 }'`
    RANCHER_FLOATING_IP=`openstack stack output show oom rancher_vm_ip -c output_value -f value`
    echo "$RANCHER_FLOATING_IP" | grep -q '[0-9]*\.[0-9]*\.[0-9]*\.[0-9]*'
    if [ ! $? -eq 0 ]; then
        sleep 8;
        continue;
    fi
    VM_IP=`ssh -i ~/.ssh/onap_key ubuntu@$RANCHER_FLOATING_IP sudo -i -u root kubectl get svc -n onap | sed -ne 's/^portal-app[ ]*LoadBalancer[ ]*[0-9\.]*[ ]*\([0-9\.]*\)[ ]*.*$/\1/p'`
    echo "$VM_IP" | grep -q '[0-9]*\.[0-9]*\.[0-9]*\.[0-9]*'
    if [ ! $? -eq 0 ]; then
        sleep 8;
        continue;
    fi
    PORTAL_IP=`openstack server list | sed -ne "s/^.*=${VM_IP},[ ]*\([0-9\.]*\)[ ]*.*$/\1/p"`
    echo "$PORTAL_IP" | grep -q '[0-9]*\.[0-9]*\.[0-9]*\.[0-9]*'
    if [ ! $? -eq 0 ]; then
        sleep 8;
        continue;
    fi
    break;
done
sed -i -e "s/PORTALIP/$PORTAL_IP/" /etc/nginx/sites-available/portal-reverse-proxy
ln -s /etc/nginx/sites-available/portal-reverse-proxy /etc/nginx/sites-enabled/
systemctl restart nginx
EOF

#
# Grab ONAP deps.
#
if [ ! -d integration ]; then
    git clone "$ONAP_INTEGRATION_REPO" integration
    if [ -n "$ONAP_INTEGRATION_BRANCH" ]; then
	cd integration
	git checkout -f "$ONAP_INTEGRATION_BRANCH"
	if [ ! $? -eq 0 ]; then
	    echo "ERROR: failed to checkout integration/$ONAP_INTEGRATION_BRANCH; falling back to powder branch"
	    git checkout -f powder
	fi
	cd ..
    fi
    cp -p integration/deployment/heat/onap-oom/env/windriver/onap-oom.env $ONAP_ENV
fi

#
# Grab ONAP base images and add them to glance.
#
openstack image show xenial-server >/dev/null
if [ ! $? -eq 0 ]; then
    get_url \
	https://cloud-images.ubuntu.com/xenial/current/xenial-server-cloudimg-amd64-disk1.img \
	xenial-server-disk1.img 64
    __openstack image create --disk-format qcow2 --public \
	--file xenial-server-disk1.img xenial-server
fi
openstack image show trusty-server >/dev/null
if [ ! $? -eq 0 ]; then
    get_url \
	https://cloud-images.ubuntu.com/trusty/current/trusty-server-cloudimg-amd64-disk1.img \
	trusty-server-disk1.img 64
    __openstack image create --disk-format qcow2 --public \
	--file trusty-server-disk1.img trusty-server
fi

#
# Add the pubkey to openstack.
#
ONAP_KEY_NAME=onap
ONAP_PUBKEY_FILE=$HOME/.ssh/onap_key.pub
openstack keypair show $ONAP_KEY_NAME
if [ ! $? -eq 0 ]; then
    if [ -z "$ONAP_PUBKEY" ]; then
	ONAP_PRIVKEY_FILE=$HOME/.ssh/onap_key
	ssh-keygen -t rsa -b 2048 -N "" -f $ONAP_PRIVKEY_FILE
	__openstack keypair create --public-key $ONAP_PUBKEY_FILE $ONAP_KEY_NAME
    else
	echo "$ONAP_PUBKEY" > $ONAP_PUBKEY_FILE
	__openstack keypair create --public-key $ONAP_PUBKEY_FILE $ONAP_KEY_NAME
    fi
fi
if [ -z "$ONAP_PUBKEY" ]; then
    ONAP_PUBKEY=`cat $ONAP_PUBKEY_FILE`
fi

#
# Customize the onap heat env file.
#
ONAP_EXT_NET_ID=`openstack network show $ONAP_EXT_NET | awk '/ id / { print $4 }'`
OS_PROJECT_ID=`openstack project show $OS_PROJECT_NAME | awk '/ id / { print $4 }'`
BOSSIP=`cat /var/emulab/boot/bossip`
OS_PASSWORD_ENCRYPTED=`echo "select p.password_hash from local_user as lu left join password as p on lu.id=p.id where lu.name='$OS_USERNAME';" | mysql -N keystone`
export OS_PASSWORD_ENCRYPTED
export OS_PROJECT_ID

sed -ie 's/apt_proxy:.*$/apt_proxy: ""/' $ONAP_ENV
sed -ie 's/docker_proxy:.*$/docker_proxy: nexus3.onap.org:10001/' $ONAP_ENV

sed -ie 's/helm_deploy_delay:.*$/helm_deploy_delay: 0.5m/' $ONAP_ENV

sed -ie "s/public_net_name:.*$/public_net_name: ${ONAP_EXT_NET}/" $ONAP_ENV
sed -ie "s/public_net_id:.*$/public_net_id: ${ONAP_EXT_NET_ID}/" $ONAP_ENV
sed -ie "s|oam_network_cidr:.*$|oam_network_cidr: 10.1.0.0/16|" $ONAP_ENV
sed -ie "s/openStackOamNetworkCidrPrefix:.*$/openStackOamNetworkCidrPrefix: 10.1/" $ONAP_ENV

sed -ie 's/ubuntu_1404_image:.*$/ubuntu_1404_image: trusty-server/' $ONAP_ENV
sed -ie 's/ubuntu_1604_image:.*$/ubuntu_1604_image: xenial-server/' $ONAP_ENV
sed -ie 's/ubuntu14Image:.*$/ubuntu14Image: trusty-server/' $ONAP_ENV
sed -ie 's/ubuntu16Image:.*$/ubuntu16Image: xenial-server/' $ONAP_ENV

sed -ie 's/rancher_vm_flavor:.*$/rancher_vm_flavor: m1.large/' $ONAP_ENV
sed -ie 's/k8s_vm_flavor:.*$/k8s_vm_flavor: m1.xlarge/' $ONAP_ENV
sed -ie 's/etcd_vm_flavor:.*$/etcd_vm_flavor: m1.medium/' $ONAP_ENV
sed -ie 's/orch_vm_flavor:.*$/orch_vm_flavor: m1.medium/' $ONAP_ENV

sed -ie "s/replicaCount:.*\$/replicaCount: ${ONAP_REPLICAS}/" $ONAP_ENV
if [ $ONAP_REPLICAS -gt 1 ]; then
    sed -ie 's/enableClustering:.*$/enableClustering: true/' $ONAP_ENV
else
    sed -ie 's/enableClustering:.*$/enableClustering: false/' $ONAP_ENV
fi

grep -q key_name: $ONAP_ENV
if [ $? -eq 0 ]; then
    sed -ie "s|key_name:.*$|key_name: ${ONAP_KEY_NAME}|" $ONAP_ENV
else
    sed -ie "s|^parameters:.*$|parameters:\n\n  key_name: ${ONAP_KEY_NAME}\n|" $ONAP_ENV
fi
sed -ie "s|vnfPubKey:.*$|vnfPubKey: $ONAP_PUBKEY|" $ONAP_ENV

sed -ie "s|KeyStoneUrl:.*$|KeyStoneUrl: \"${OS_AUTH_URL}\"|" $ONAP_ENV

LOCALDNS=`cat /etc/resolv.conf | grep -v 192.168 | grep nameserver | cut -d' ' -f2`
if [ -n "$LOCALDNS" ]; then
    grep -q dns_list $ONAP_ENV
    if [ $? -eq 0 ]; then
	sed -ie "s|dns_list:.*$|dns_list: [ \"$LOCALDNS\" ]|" $ONAP_ENV
    else
	echo "  dns_list: [ \"$LOCALDNS\" ]" >> $ONAP_ENV
    fi
    grep -q external_dns $ONAP_ENV
    if [ $? -eq 0 ]; then
	sed -ie "s|external_dns:.*$|external_dns: $LOCALDNS|" $ONAP_ENV
    else
	echo "  external_dns: $LOCALDNS" >> $ONAP_ENV
    fi
    grep -q dns_forwarder $ONAP_ENV
    if [ $? -eq 0 ]; then
	sed -ie "s|dns_forwarder:.*$|dns_forwarder: $LOCALDNS|" $ONAP_ENV
    else
	echo "  dns_forwarder: $LOCALDNS" >> $ONAP_ENV
    fi
fi

#
# Patch heat to perform floating IP alloc retries.  Our pools may be
# small (i.e. on the ext-net), and neutron's floating IP allocation
# process cannot handle small pools.  It apparently doesn't lock the
# ipallocationpools table (or anywhere) when getting an address;
# attempts to fix it assume a large pool
# (https://bugs.launchpad.net/neutron/+bug/1777968).  And Heat generally
# does not retry resource allocation
# (https://wiki.openstack.org/wiki/Support-retry-with-idempotency).
#
patch -d / -p0 < $DIRNAME/etc/heat-queens-floating-ip-retries.patch
if [ $? -eq 0 ]; then
    systemctl restart heat-api heat-engine heat-api-cfn
    sleep 8
else
    patch -d / -p0 -R < $DIRNAME/etc/heat-queens-floating-ip-retries.patch
    patch -d / -p0 < $DIRNAME/etc/heat-ocata-floating-ip-retries.patch
    if [ $? -eq 0 ]; then
	systemctl restart heat-api heat-engine heat-api-cfn
	sleep 8
    else
	patch -d / -p0 -R < $DIRNAME/etc/heat-ocata-floating-ip-retries.patch
    fi
fi

#
# Fix up Ocata keystone config to avoid heat stack delete errors; this
# is probably a packaging bug.  The memcache driver doesn't appear to be
# packaged, which is a mistake.
#
if [ $OSVERSION -eq $OSOCATA ]; then
    crudini --set /etc/keystone/keystone.conf token driver kvs
    systemctl restart apache2
    sleep 16
fi

#
# Create the ONAP stack.
#
maybe_install_packages jq
cd integration/deployment/heat/onap-oom
scripts/deploy.sh $ONAP_ENV
while [ ! $? -eq 0 ]; do
    sstatus=`openstack stack show oom -f value -c stack_status`
    if [ ! "$sstatus" = "CREATE_COMPLETE" ]; then
	__openstack stack delete -y oom
	retries=120
	gone=0
	while [ $retries -gt 0 ]; do
	    sleep 8
	    __openstack stack show oom -f value -c stack_status
	    # Wait until it's gone (at which point it returns error).
	    if [ ! $? -eq 0 ]; then
		gone=1
		break
	    fi
	    retries=`expr $retries - 1`
	done
	if [ $gone -eq 1 ]; then
	    echo "ONAP deploy.sh failed at stack deploy; retrying" | mail -s "ONAP Deployment Failed: Retrying" ${SWAPPER_EMAIL}
	    scripts/deploy.sh $ONAP_ENV
	else
	    echo "ONAP deploy.sh failed at stack deploy, but stack teardown failed; not retrying.  You will need to investigate manually, or create a new experiment." | mail -s "ONAP Deployment Failed: Not Retrying" ${SWAPPER_EMAIL}
	    exit 1
	fi
    else
	echo "ONAP deploy.sh failed after stack deploy!  You will need to investigate in more detail; login to `hostname`, become root, and look at $OURDIR/setup-ext-onap.log .  Not preloading demo content." | mail -s "ONAP Deployment Failed: Not Retrying" ${SWAPPER_EMAIL}
	exit 1
    fi
done

#
# Collect some basic info (and maybe preload the demo info).
#
RANCHERIP=
while [ -z "$RANCHERIP" ]; do
    echo "Waiting for Rancher VM floating IP..."
    sleep 8
    #RANCHERIP=`openstack server list | sed -ne 's/^.* oom-rancher.*, \([0-9\.]*\) .*$/\1/p'`
    RANCHERIP=`openstack stack output show oom rancher_vm_ip -c output_value -f value`
done
echo $RANCHERIP > $OURDIR/rancher-floating-ip

ROBOTPOD=
while [ -z "$ROBOTPOD" ]; do
    echo "Waiting for Robot pod existence..."
    sleep 8
    ROBOTPOD=`ssh -q -i ~/.ssh/onap_key -o StrictHostKeyChecking=no -t ubuntu@$RANCHERIP sudo -in kubectl get pods -n onap | grep robot | grep -i Running | awk '/ / { print $1 }'`
done
echo $ROBOTPOD > $OURDIR/robot-pod

VMIP=
while [ -z "$VMIP" ]; do
    VMIP=`ssh -q -i ~/.ssh/onap_key -o StrictHostKeyChecking=no -t ubuntu@$RANCHERIP sudo -i kubectl get svc -n onap | sed -ne 's/^portal-app[ ]*LoadBalancer[ ]*[0-9\.]*[ ]*\([0-9\.]*\)[ ]*.*$/\1/p'`
    echo "$VMIP" | grep -q '[0-9]*\.[0-9]*\.[0-9]*\.[0-9]*'
    if [ ! $? -eq 0 ]; then
	VMIP=
	echo "Waiting for k8s VM IP..."
	sleep 8
	continue
    fi
done
echo $VMIP > $OURDIR/k8s-internal-ip

PORTALIP=
while [ -z "$PORTALIP" ]; do
    PORTALIP=`openstack server list | sed -ne "s/^.*=${VMIP},[ ]*\([0-9\.]*\)[ ]*.*$/\1/p"`
    echo "$PORTALIP" | grep -q '[0-9]*\.[0-9]*\.[0-9]*\.[0-9]*'
    if [ ! $? -eq 0 ]; then
	PORTALIP=
	echo "Waiting for ONAP Portal IP (k8s floating IP)..."
        sleep 8
        continue
    fi
done
echo $PORTALIP > $OURDIR/portal-floating-ip

cat <<EOF >$OURDIR/onap.mail
Your ONAP instance has completed setup!  Once you have modified your
machine's /etc/hosts file with the contents below, you can browse to
https://portal.api.simpledemo.onap.org:30225/ONAPPORTAL/login.htm and
login with the 'demo' username and 'demo123456!' password.

If you selected the option to preload demo information, that process
is continuing.  Wait for the next email to tell you that has finished.

ONAP Portal floating IP: $PORTALIP
ONAP Rancher VM floating IP: $RANCHERIP

/etc/hosts file contents:

$PORTALIP     api.simpledemo.onap.org
$PORTALIP     portal.api.simpledemo.onap.org
$PORTALIP     portal-app.onap
$PORTALIP     vid.api.simpledemo.onap.org
$PORTALIP     sdc.api.fe.simpledemo.onap.org
$PORTALIP     sdc.api.simpledemo.onap.org
$PORTALIP     portal-sdk.simpledemo.onap.org
$PORTALIP     policy.api.simpledemo.onap.org
$PORTALIP     aai.api.sparky.simpledemo.onap.org
$PORTALIP     cli.api.simpledemo.onap.org
$PORTALIP     msb.api.discovery.simpledemo.onap.org
$PORTALIP     sdc.dcae.plugin.simpledemo.onap.org
$PORTALIP     sdc.workflow.plugin.simpledemo.onap.org
$PORTALIP     clamp.api.simpledemo.onap.org
EOF
cat $OURDIR/onap.mail | mail -s "ONAP Instance Finished Setting Up" ${SWAPPER_EMAIL}

if [ $ONAP_PRELOAD_DEMOS -eq 1 ]; then
    ssh -q -i ~/.ssh/onap_key -o StrictHostKeyChecking=no -t ubuntu@$RANCHERIP sudo -in "kubectl exec -it $ROBOTPOD -- /bin/sh -c 'cd /var/opt/OpenECOMP_ETE && rm -rf demo && git clone -b $ONAP_DEMO_BRANCH $ONAP_DEMO_REPO demo && exit'"

    ssh -q -i ~/.ssh/onap_key -o StrictHostKeyChecking=no -t ubuntu@$RANCHERIP \
	'sudo -in bash -c "echo demo123456! | oom/kubernetes/robot/demo-k8s.sh onap init_robot"'

    #ssh -q -i ~/.ssh/onap_key -o StrictHostKeyChecking=no -t ubuntu@$RANCHERIP \
    #    sudo -in bash oom/kubernetes/robot/ete-k8s.sh onap health
    ssh -q -i ~/.ssh/onap_key -o StrictHostKeyChecking=no -t ubuntu@$RANCHERIP \
	sudo -in bash oom/kubernetes/robot/demo-k8s.sh onap init_customer

    curl -k -X POST https://$PORTALIP:30200/vid/maintenance/category_parameter/owningEntity \
	 -H 'Accept: application/json' -H 'Content-Type: application/json' \
	 -H 'Postman-Token: 6a3181fa-c720-4c19-a952-79d68c9435cc' -H 'USER_ID: demo' \
	 -H 'X-FromAppId: robot-ete' -H 'X-TransactionId: robot-ete-bd65600d-8669-4903-8a14-af88203add38' \
	 -H 'cache-control: no-cache' -d '{ "options": ["OE-Demonstration"] }'
    curl -k -X POST https://$PORTALIP:30200/vid/maintenance/category_parameter/project \
	 -H 'Accept: application/json' -H 'Content-Type: application/json' \
	 -H 'Postman-Token: 6a3181fa-c720-4c19-a952-79d68c9435cc' -H 'USER_ID: demo' \
	 -H 'X-FromAppId: robot-ete' -H 'X-TransactionId: robot-ete-bd65600d-8669-4903-8a14-af88203add38' \
	 -H 'cache-control: no-cache' -d '{ "options": ["Project-Demonstration"] }'
    curl -k -X POST https://$PORTALIP:30200/vid/maintenance/category_parameter/platform \
	 -H 'Accept: application/json' -H 'Content-Type: application/json' \
	 -H 'Postman-Token: 6a3181fa-c720-4c19-a952-79d68c9435cc' -H 'USER_ID: demo' \
	 -H 'X-FromAppId: robot-ete' -H 'X-TransactionId: robot-ete-bd65600d-8669-4903-8a14-af88203add38' \
	 -H 'cache-control: no-cache' -d '{ "options": ["Platform-Demonstration"] }'

    ssh -q -i ~/.ssh/onap_key -o StrictHostKeyChecking=no -t ubuntu@$RANCHERIP \
	sudo -in bash oom/kubernetes/robot/demo-k8s.sh onap distribute
    ssh -q -i ~/.ssh/onap_key -o StrictHostKeyChecking=no -t ubuntu@$RANCHERIP \
        sudo -in bash oom/kubernetes/robot/demo-k8s.sh onap instantiateVFW

    echo "The ONAP demo info has finished preloading.  You should be able to instantiate a vFW VNF element." | mail -s "ONAP Demos Finished Preloading" ${SWAPPER_EMAIL}
fi

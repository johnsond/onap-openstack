This is a CloudLab/Emulab profile that sets up ONAP
(https://www.onap.org/) atop the CloudLab/Emulab OpenStack profile
(https://www.cloudlab.us/p/emulab-ops/OpenStack ,
https://gitlab.flux.utah.edu/johnsond/openstack-build-ubuntu).
